# 23 Key Macro Pad

This project is my first serious attempt at making a "keyboard" from scratch. To fit within the $5 tier on PCBWay and my small 3D printer the design will be limited to approximately 10cm by 10cm, which means I could only get 23 Cherry MX style switches to fit.
